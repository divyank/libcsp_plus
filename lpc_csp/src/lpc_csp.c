/*
===============================================================================
 Name        : lpc_csp.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
// TODO: insert other include files here
#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>

#include <csp/drivers/usart.h>
#include <csp/arch/csp_thread.h>
#include <house_keeper.h>
// TODO: insert other definitions and declarations here
#define PORT 10
#define LPC_ADDRESS 1
#define PC_ADDRESS 8


CSP_DEFINE_TASK(task_client) {

    const char* outbuf = "This is my message to the ground!";
    char inbuf[32] = {'x',0};
    int pingResult;
    char hk_outbuf[sizeof(eps_hk_t) + 1];
    eps_hk_t* hk_data = hk_outbuf + 1;
    HouseKeeper_setTestValues(hk_data);
    hk_outbuf[0] = 'h';


    //int32_t start;
    //int32_t end;

    size_t loop_count = 0;
    while (true){
		loop_count++;
		printf("Client task loop #: %d\n", loop_count);
		printf("Size of eps_hk_t:%d\n", sizeof(eps_hk_t));
		// the largest ping payload that works is 246 bytes.
		// This seems to be related to the size of the send ring buffer set in the usart_lpc1769.c driver in libcsp
		// As at the time this comment was written it was set to 256:
        // #define UART_SRB_SIZE 256
		// Setting this to 128 causes a ping failure at about 124 bytes.
		// Note that the KISS MTU is set to 256, so this may have an influence as well.
		for(int i = 46; i <= 246; i+= 50) {
			// send a ping, to MY_ADDRESS, 1000 ms timeout, i byte packet, no connection options
			// pingResult of -1 is an error.
			pingResult = csp_ping(PC_ADDRESS, 1000, i, CSP_O_NONE);
			printf("Ping with payload of %d bytes, took %d ms\n", i, pingResult);
			csp_sleep_ms(1);
		}
		// requesting info about the os the node is running on (linux in this case)
		// Each call prints to the console
		csp_ps(PC_ADDRESS, 1000); // request process list (which is not available on linux);1000ms timeout
		//start = csp_get_ms();
		csp_sleep_ms(1); // seems to stop much longer than 1 ms - a sleep of 1 takes 1000 ms;
		//end = csp_get_ms();
		//printf("sleep of 1 took %d ms", (end - start));
		csp_memfree(PC_ADDRESS, 1000); // request amount of free memory for a node. 1000ms timeout
		csp_sleep_ms(1);
		csp_buf_free(PC_ADDRESS, 1000); // request number of free buffers for a node, 1000 ms timeout
		csp_sleep_ms(1);
		csp_uptime(PC_ADDRESS, 1000); // request uptime
		//Perform an entire request/reply transaction
		// * Copies both input buffer and reply to output buffeer.
		// * Also makes the connection and closes it again
		// priority 0, dest, port, timeout, output buffer, length of output, input buffer, input length
		csp_sleep_ms(1);
		csp_transaction(0, PC_ADDRESS, PORT, 1000, &outbuf, strlen(outbuf), inbuf, 25);
		printf("Test response from server: %s\n", inbuf);
        // Send a struct
		csp_sleep_ms(1);
		csp_transaction(0, PC_ADDRESS, PORT, 1000, hk_outbuf, sizeof(eps_hk_t) + 1, inbuf, 25);
		printf("Test response from server: %s\n", inbuf);
		puts(inbuf);
    }
    return CSP_TASK_RETURN;
}


int main(void) {

#if defined (__USE_LPCOPEN)
#if !defined(NO_BOARD_LIB)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
    // Set the LED to the state of "On"
    Board_LED_Set(0, true);
#endif
#endif

    // TODO: insert code here
    csp_debug_toggle_level(CSP_PACKET);
    csp_debug_toggle_level(CSP_INFO);

    // Start buffer handling;  10 buffers, 300 bytes long each
    csp_buffer_init(10, 300);
    // start up CSP
    csp_init(LPC_ADDRESS);

    struct usart_conf conf;
    conf.device = "";
    conf.baudrate = 9600;
    conf.databits = 8;
    conf.paritysetting = 0;
    conf.stopbits = 1;
    conf.checkparity = 0;

	/* Run USART init */
	usart_init(&conf);

    /* Setup CSP interface */
	static csp_iface_t csp_if_kiss;
	static csp_kiss_handle_t csp_kiss_driver;
	// initialize the KISS interface
	csp_kiss_init(&csp_if_kiss, &csp_kiss_driver, usart_putc, usart_insert, "KISS");

	/* Setup callback from USART RX to KISS RS */
	void my_usart_rx(uint8_t * buf, int len, void * pxTaskWoken) {
		csp_kiss_rx(&csp_if_kiss, buf, len, pxTaskWoken);
	}
	usart_set_callback(my_usart_rx);

    //csp_route_set(MY_ADDRESS, &csp_if_kiss, CSP_NODE_MAC);
    //csp_route_set(LPC_ADDRESS, &csp_if_kiss, PC_ADDRESS);
    csp_route_set(PC_ADDRESS, &csp_if_kiss, CSP_NODE_MAC);
    //csp_route_set(PC_ADDRESS, &csp_if_kiss, LPC_ADDRESS);
    csp_route_start_task(200, 1);

    //csp_conn_print_table();
    //csp_route_print_table();
    //csp_route_print_interfaces();

    // start the server task
    //csp_thread_handle_t handle_server;
    //csp_thread_create(task_server, (signed char *) "SERVER", 1000, NULL, 0, &handle_server);
    csp_thread_handle_t handle_client;
    csp_thread_create(task_client, (signed char *) "CLIENT", 1000, NULL, 0, &handle_client);

    /* Start the scheduler so our tasks start executing. */
    vTaskStartScheduler();
    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; );
    return 0;
}

void vApplicationIdleHook( void )
{
     /* This example does not use the idle hook to perform any processing. */
}

